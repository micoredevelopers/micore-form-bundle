<?php


namespace MiCore\FormBundle\Form;


use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class FormFactorySingleton  implements  FormFactoryInterface
{

    private $forms = [];
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var \SplObjectStorage
     */
    private $splObjectStorage;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
        $this->splObjectStorage = new \SplObjectStorage();
    }

    public function create(string $type = 'Symfony\Component\Form\Extension\Core\Type\FormType', $data = null, array $options = [])
    {
        return $this->getForm($type, 'create', [$type, $data, $options]);
    }

    public function createNamed(string $name, string $type = 'Symfony\Component\Form\Extension\Core\Type\FormType', $data = null, array $options = [])
    {
        return $this->getForm($type, 'createNamed', [$name, $type, $data, $options]);
    }

    public function createForProperty(string $class, string $property, $data = null, array $options = [])
    {
        return $this->getForm($class, 'createForProperty', [$class, $property, $data, $options]);
    }

    public function createBuilder(string $type = 'Symfony\Component\Form\Extension\Core\Type\FormType', $data = null, array $options = [])
    {
        return $this->getForm($type, 'createBuilder', [$type, $data, $options]);
    }

    public function createNamedBuilder(string $name, string $type = 'Symfony\Component\Form\Extension\Core\Type\FormType', $data = null, array $options = [])
    {
        return $this->getForm($type, 'createNamedBuilder', [$name, $type, $data, $options]);
    }

    public function createBuilderForProperty(string $class, string $property, $data = null, array $options = [])
    {
        return $this->getForm($class, 'createBuilderForProperty', [$class, $property, $data, $options]);
    }

    /**
     * @param string $type
     * @param string $method
     * @param array $params
     * @return FormInterface
     */
    private function getForm(string $type, string $method, array $params): FormInterface
    {
        if (!isset($this->forms[$type])){
            $this->forms[$type] = call_user_func_array([$this->formFactory, $method], $params);
        }
        return $this->forms[$type];
    }
}
