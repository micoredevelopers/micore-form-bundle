<?php


namespace MiCore\FormBundle\Form\Model;

class DateTimeRange
{
    /**
     * @var \DateTimeInterface|null
     */
    private $from;

    /**
     * @var \DateTimeInterface|null
     */
    private $to;

    /**
     * @var \DateTimeInterface|null
     */
    private $equal;


    /**
     * @return \DateTimeInterface|null
     */
    public function getFrom(): ?\DateTimeInterface
    {
        return $this->from;
    }

    /**
     * @param \DateTimeInterface|null $from
     * @return $this
     */
    public function setFrom(?\DateTimeInterface $from): self
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTo(): ?\DateTimeInterface
    {
        return $this->to;
    }

    /**
     * @param \DateTimeInterface|null $to
     * @return $this
     */
    public function setTo(?\DateTimeInterface $to): self
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEqual(): ?\DateTimeInterface
    {
        return $this->equal;
    }

    /**
     * @param \DateTimeInterface|null $equal
     * @return $this
     */
    public function setEqual(?\DateTimeInterface $equal): self
    {
        $this->equal = $equal;
        return $this;
    }

    /**
     * @param \DateTimeInterface ...$dates
     * @return static
     */
    public static function createFromDates(...$dates): self
    {

        $result = new self();

        $from = $to = null;
        foreach ($dates as $date){

             if (null === $from && null === $to){
                $from = $to = $date;
                continue;
            }

            $timestamp = $date->getTimestamp();
            if ($timestamp > $to->getTimestamp()){
                $to = $date;
            }
            if ($timestamp < $from->getTimestamp()){
                $from = $date;
            }
        }

        if ($from === $to){
            return $result->setEqual($from);
        }

        return $result->setTo($to)->setFrom($from);
    }

}
