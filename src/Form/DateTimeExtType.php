<?php


namespace MiCore\FormBundle\Form;


use MiCore\FormBundle\Form\DataTransformer\DateTimeFromStringTransformerFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateTimeExtType extends AbstractType
{

    /**
     * @var DateTimeFromStringTransformerFactory
     */
    private $dateTimeFromStringTransformerFactory;
    /**
     * @var string
     */
    private $format;
    /**
     * @var string
     */
    private $formatWithTime;
    /**
     * @var string
     */
    private $delimiter;

    /**
     * DateTimeType constructor.
     * @param string $format
     * @param string $formatWithTime
     * @param string $delimiter
     * @param DateTimeFromStringTransformerFactory $dateTimeFromStringTransformerFactory
     */
    public function __construct(string $format, string $formatWithTime,  string $delimiter, DateTimeFromStringTransformerFactory $dateTimeFromStringTransformerFactory)
    {
        $this->dateTimeFromStringTransformerFactory = $dateTimeFromStringTransformerFactory;
        $this->format = $format;
        $this->formatWithTime = $formatWithTime;
        $this->delimiter = $delimiter;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if (null === $options['format']){
            $options['format'] = false === $options['without_time'] ? $this->formatWithTime : $this->format;
        }
        if (null === $options['delimiter'] && true === $options['range']){
            $options['delimiter'] = $this->delimiter;
        }

        parent::buildForm($builder, $options);
        $builder->addViewTransformer($this->dateTimeFromStringTransformerFactory->create($options['format'], $options['delimiter'], $options['without_time']));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
       $resolver->setDefaults([
           'format' => null,
           'delimiter' => null,
           'without_time' => true,
           'range' => false,
           'compound' => false
       ]);
    }

}
