<?php


namespace MiCore\FormBundle\Form\Extension;


use MiCore\FormBundle\Form\DataTransformer\ArrayTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StringToArrayExtension extends AbstractTypeExtension
{

    /**
     * @var ArrayTransformer
     */
    private $arrayTransformer;

    public function __construct(ArrayTransformer $arrayTransformer)
    {
        $this->arrayTransformer = $arrayTransformer;
    }

    public static function getExtendedTypes(): iterable
    {
        return [EntityType::class, ChoiceType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (true === $options['multiple'] && null !== $options['delimiter']) {
            $builder->addViewTransformer(new ArrayTransformer($options['delimiter']));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['delimiter' => ',']);
    }
}
