<?php


namespace MiCore\FormBundle\Form;


use MiCore\FormBundle\Form\DataTransformer\BoolTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoolExtType extends AbstractType
{

    /**
     * @var BoolTransformer
     */
    private $boolTransformer;

    public function __construct(BoolTransformer $boolTransformer)
    {
        $this->boolTransformer = $boolTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer($this->boolTransformer);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'compound' => false
        ]);
    }

}
