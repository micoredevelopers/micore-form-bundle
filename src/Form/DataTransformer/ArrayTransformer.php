<?php


namespace MiCore\FormBundle\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ArrayTransformer implements DataTransformerInterface
{

    /**
     * @var string
     */
    private $delimiter;

    public function __construct(string $delimiter = ',')
    {
        $this->delimiter = $delimiter;
    }

    public function transform($value)
    {
       return $value;
    }

    /**
     * @param mixed $value
     * @return array|false|mixed|string[]|null
     */
    public function reverseTransform($value)
    {
        if (!$value){
            return null;
        }
        if (is_iterable($value) || null === $value){
            return $value;
        } else if (is_string($value)){
            return explode($this -> delimiter, $value);
        }
        throw new TransformationFailedException;
    }
}
