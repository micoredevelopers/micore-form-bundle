<?php


namespace MiCore\FormBundle\Form\DataTransformer;


use MiCore\FormBundle\Form\Model\FloatRange;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FloatRangeTransformer implements DataTransformerInterface
{

    /**
     * @var string
     */
    private $delimiter;

    public function __construct($delimiter = '-')
    {
        $this->delimiter = $delimiter;
    }

    public function transform($value)
    {
       return $value;
    }

    /**
     * @param mixed $value
     * @return FloatRange|mixed|null
     */
    public function reverseTransform($value)
    {

        if (!$value){
            return null;
        }

        if (!is_string($value) && !is_float($value)){
            throw new TransformationFailedException();
        }

        $exp = explode($this->delimiter, $value);
        if (count($exp) > 2){
            throw new TransformationFailedException();
        }

        foreach ($exp as $value){
            if (!is_numeric($value)){
                throw new TransformationFailedException();
            }
        }

        return new FloatRange($exp[0], $exp[1] ?? null);

    }
}
