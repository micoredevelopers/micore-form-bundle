<?php


namespace MiCore\FormBundle\Form\DataTransformer;


use Symfony\Component\Validator\Validator\ValidatorInterface;

class DateTimeFromStringTransformerFactory
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param string $format
     * @param string|null $delimiter
     * @param bool $withoutTime
     * @return DateTimeFromStringTransformer
     */
    public function create(string $format, ?string $delimiter,  bool $withoutTime)
    {
        return new DateTimeFromStringTransformer($format, $delimiter, $withoutTime, $this->validator);
    }
}
