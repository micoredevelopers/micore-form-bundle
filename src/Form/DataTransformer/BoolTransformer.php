<?php


namespace MiCore\FormBundle\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class BoolTransformer implements DataTransformerInterface
{

    public function transform($value)
    {
        return $value;
    }

    public function reverseTransform($value)
    {
        if (null === $value || is_bool($value)){
            return $value;
        } elseif (is_scalar($value)){
            $value = (int)$value;
            switch ($value) {
                case 1:
                    return true;
                case 0:
                    return false;
                default:
                    throw new TransformationFailedException;
            }
        }
        throw new TransformationFailedException;
    }
}
