<?php


namespace MiCore\FormBundle\Form\DataTransformer;


use MiCore\FormBundle\Form\Model\DateTimeRange;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DateTimeFromStringTransformer implements DataTransformerInterface
{

    /**
     * @var string
     */
    private $format;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var array
     */
    private $constraints;
    /**
     * @var string|null
     */
    private $delimiter;
    /**
     * @var bool
     */
    private $withoutTime;

    public function __construct(string $format, ?string $delimiter, bool $withoutTime, ValidatorInterface $validator)
    {
        $this->format = $format;
        $this->validator = $validator;
        $this->constraints = [new DateTime(['format' => $format])];
        $this->delimiter = $delimiter;
        $this->withoutTime = $withoutTime;
    }


    /**
     * @param string $dateTime
     * @return \DateTimeInterface
     */
    private function createDate($dateTime): ?\DateTimeInterface
    {
        if (!$dateTime) {
            return null;
        }

        $errors = $this->validator->validate($dateTime, $this->constraints);
        if (count($errors)) {
            throw new TransformationFailedException();
        }
        $dateTime = \DateTime::createFromFormat($this->format, $dateTime);
        if (true === $this->withoutTime){
            $f = $dateTime->format('Y-m-d').' 00:00:00';
            $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $f);
        }
        return $dateTime;
    }

    /**
     * @param $dateTime
     * @return string|null
     */
    private function createDateString($dateTime): ?string
    {
        if (!$dateTime) {
            return null;
        }
        if ($dateTime instanceof \DateTimeInterface) {
            return $dateTime->format($this->format);
        }
        throw new TransformationFailedException();
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * @param mixed $value
     * @return \DateTimeInterface|mixed|null
     */
    public function reverseTransform($value)
    {

        if (!$value) {
            return null;
        }
        if ($this->delimiter){
            if (!is_string($value)){
                throw new TransformationFailedException();
            }
            $dates = [];
            foreach (explode($this->delimiter, $value) as $dateString){
                $d = $this->createDate($dateString);
                if (!$d){
                    continue;
                }
                $dates[] = $d;
            }
            if (!count($dates) || count($dates) > 2){
                throw new TransformationFailedException();
            }
           return DateTimeRange::createFromDates(...$dates);
        } else {
            return $this->createDate($value);
        }
    }
}
