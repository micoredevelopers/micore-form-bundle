<?php


namespace MiCore\FormBundle\DependencyInjection;

use MiCore\FormBundle\Form\BoolExtType;
use MiCore\FormBundle\Form\DataTransformer\ArrayTransformer;
use MiCore\FormBundle\Form\DataTransformer\BoolTransformer;
use MiCore\FormBundle\Form\DataTransformer\DateTimeFromStringTransformerFactory;
use MiCore\FormBundle\Form\DataTransformer\FloatRangeTransformer;
use MiCore\FormBundle\Form\DateTimeExtType;
use MiCore\FormBundle\Form\Extension\StringToArrayExtension;
use MiCore\FormBundle\Form\FloatRangeExtType;
use MiCore\FormBundle\Form\FormFactorySingleton;
use MiCore\FormBundle\Form\StringArrayExtType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class MiCoreFormExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {

        $configs = $this->processConfiguration(new Configuration(), $configs);

        $container->autowire(BoolExtType::class)->addTag('form.type');
        $container->autowire(FloatRangeExtType::class)->addTag('form.type');
        $container->autowire(StringArrayExtType::class)->addTag('form.type');
        $container->autowire(StringToArrayExtension::class)->addTag('form.type_extension');
        $container->autowire(ArrayTransformer::class);
        $container->autowire(BoolTransformer::class);
        $container->autowire(DateTimeFromStringTransformerFactory::class);
        $container->autowire(FloatRangeTransformer::class);
        $container->autowire(FormFactorySingleton::class);
        $container->autowire(DateTimeExtType::class)
            ->addTag('form.type')
            ->setArguments([
                '$format' => $configs['date']['format'],
                '$formatWithTime' => $configs['date']['time_format'],
                '$delimiter' => $configs['date']['range_delimiter']
            ]);

    }
}
