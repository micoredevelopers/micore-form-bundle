<?php


namespace MiCore\FormBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('mi_core_form');
        $treeBuilder->getRootNode()
            ->children()
              ->arrayNode('date')->addDefaultsIfNotSet()
                  ->children()
                     ->scalarNode('format')->defaultValue('Y-m-d')->end()
                     ->scalarNode('time_format')->defaultValue('Y-m-d H:i')->end()
                     ->scalarNode('range_delimiter')->defaultValue('/')->end()
                  ->end()
              ->end()
            ->end();

        return $treeBuilder;
    }
}
