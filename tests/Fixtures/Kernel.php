<?php


namespace MiCore\FormBundle\Tests\Fixtures;


use MiCore\FormBundle\MiCoreFormBundle;
use MiCore\KernelTest\KernelTest;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;

class Kernel extends KernelTest
{

    public function configureContainer(ContainerBuilder $c)
    {
        $c->autowire(FixtureType::class)->addTag('form.type');
        $c->autowire(FormFactoryInterface::class, FormFactory::class)->setPublic(true);
    }

    protected function bundles(): array
    {
        return [
            MiCoreFormBundle::class
        ];
    }
}
