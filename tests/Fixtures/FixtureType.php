<?php


namespace MiCore\FormBundle\Tests\Fixtures;


use MiCore\FormBundle\Form\BoolExtType;
use MiCore\FormBundle\Form\DateTimeExtType;
use MiCore\FormBundle\Form\FloatRangeExtType;
use MiCore\FormBundle\Form\StringArrayExtType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FixtureType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('foo_0', BoolExtType::class, $options['bool_options'])
            ->add('foo_1', DateTimeExtType::class, $options['date_time_options'])
            ->add('foo_2', FloatRangeExtType::class, $options['float_range_options'])
            ->add('foo_3', StringArrayExtType::class, $options['string_array_options'])
            ->add('foo_4', ChoiceType::class, $options['choice_options'])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'bool_options' => [],
                'date_time_options' => [],
                'float_range_options' => [],
                'string_array_options' => [],
                'choice_options' => ['choices' => [1,2,3,4,5,6], 'multiple' => true]
            ]);
    }

}
