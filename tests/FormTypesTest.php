<?php


namespace MiCore\FormBundle\Tests;


use MiCore\FormBundle\Form\Model\DateTimeRange;
use MiCore\FormBundle\Form\Model\FloatRange;
use MiCore\FormBundle\Tests\Fixtures\FixtureType;
use MiCore\KernelTest\KernelTestCase;
use Symfony\Component\Form\FormFactoryInterface;

class FormTypesTest extends KernelTestCase
{

    /**
     * @return FormFactoryInterface
     */
    private function getFormFactory(): FormFactoryInterface
    {
        return self::$container->get(FormFactoryInterface::class);
    }

    /**
     * @param array $fooInput
     * @param array $fooOutput
     * @param array $options
     * @dataProvider dataProviderTestDataTransform
     */
    public function testDataTransform(array $fooInput, array $fooOutput = [], array $options = [])
    {
        $form = $this->getFormFactory()->create(FixtureType::class, null, $options);

        $form->submit($fooInput);
        foreach ($fooOutput as $name => $value) {
            $realVal = $form->get($name)->getData();
            $this->assertEquals($value, $realVal);;
        }
    }

    /**
     * @param $data
     * @param $minAssert
     * @param $maxAssert
     * @param string $delimiter
     *
     * @dataProvider dataProviderTestDataTransformFloatRange
     */
    public function testDataTransformFloatRange($data, $minAssert, $maxAssert, $delimiter = '-')
    {
        $form = $this->getFormFactory()->create(FixtureType::class, null, ['float_range_options' => ['delimiter' => $delimiter]]);
        $form->submit(['foo_2' => $data]);

        /**
         * @var FloatRange $range
         */
        $range = $form->get('foo_2')->getData();
        $this->assertEquals($range->getMin(), $minAssert);
        $this->assertEquals($range->getMax(), $maxAssert);
    }

    /**
     * @param $date
     * @param null $eqAssert
     * @param null $fromAssert
     * @param null $toAssert
     * @param array $options
     *
     * @dataProvider dataProviderTestDataTransformDateType
     */
    public function testDataTransformDateType($date, $eqAssert = null, $fromAssert = null, $toAssert = null, $options = [])
    {
        $form = $this->getFormFactory()->create(FixtureType::class, null, ['date_time_options' => $options]);
        $form->submit(['foo_1' => $date]);

        $range = (isset($options['range']) && $options['range'])  || (isset($options['delimiter']) && $options['delimiter']);
        $data = $form->get('foo_1')->getData();

        if ($range){

            if ($eqAssert !== null || $fromAssert !== null || $toAssert = null) {
                $this->assertInstanceOf(DateTimeRange::class, $data);
                if (null === $eqAssert){
                    $this->assertNull($data->getEqual());
                } else {
                    $this->assertEquals($eqAssert, $data->getEqual()->getTimestamp());
                }

                if (null === $fromAssert){
                    $this->assertNull($data->getFrom());
                } else {
                    $this->assertEquals($fromAssert, $data->getFrom()->getTimestamp());
                }

                if (null === $toAssert){
                    $this->assertNull($data->getTo());
                } else {
                    $this->assertEquals($toAssert, $data->getTo()->getTimestamp());
                }
            } else {
                $this->assertNull($data);
            }

        } else {

            if (null === $eqAssert){
                $this->assertNull($data);
            } else {
                $this->assertInstanceOf(\DateTimeInterface::class, $data);
                $this->assertEquals($eqAssert, $data->getTimestamp());
            }
        }

    }

    public function dataProviderTestDataTransformDateType()
    {
        return [
            ['2020-08-16', strtotime('2020-08-16'), null, null, ['range' => true]],
            ['2020-08-16', strtotime('2020-08-16'), null, null],
            ['2020-08-16/2020-08-15', null, strtotime('2020-08-15'), strtotime('2020-08-16'), ['range' => true]],
            ['2020-08-16/', strtotime('2020-08-16'), null, null, ['range' => true]],
            ['/2020-08-16', strtotime('2020-08-16'), null, null, ['range' => true]],
            ['/', null, null, null, ['range' => true]],


            ['2020/08/16-2020/08/15', null, strtotime('2020-08-15'), strtotime('2020-08-16'), ['delimiter' => '-', 'format' => 'Y/m/d']],
            ['2020/08/16 25:20-2020/08/15 20:30', null, strtotime('2020-08-15 20:30'), strtotime('2020-08-16 15:20'), ['delimiter' => '-', 'format' => 'Y/m/d H:i', 'without_time' => false]],
            ['2020/08/16 25:20-2020/08/15 20:30', null, null, null, ['delimiter' => '-', 'format' => 'Y/m/d H:i', 'without_time' => false]],
        ];
    }

    public function dataProviderTestDataTransformFloatRange(): array
    {
        return [
            ['1-2', 1, 2, '-'],
            ['1/2', 1, 2, '/'],
            ['1iiii2', 1, 2, 'iiii'],
            ['1', 1, 0, 'iiii'],
        ];
    }

    public function dataProviderTestDataTransform(): array
    {
        return [
            [['foo_0' => 1, 'foo_3' => '1,2,3', 'foo_4' => '1,2,3'], ['foo_0' => true, 'foo_3' => [1, 2, 3], 'foo_4' => [1, 2, 3]]],
            [['foo_0' => 0, 'foo_3' => '1|2|3'], ['foo_0' => false, 'foo_3' => ['1|2|3']]],
            [['foo_0' => 0, 'foo_3' => '1|2|3', 'foo_4' => '1|2|3'], ['foo_0' => false, 'foo_3' => [1, 2, 3], 'foo_4' => [1, 2, 3]], ['string_array_options' => ['delimiter' => '|'], 'choice_options' => ['delimiter' => '|', 'choices' => [1, 2, 3, 4, 5, 6], 'multiple' => true]]],
        ];
    }
}
